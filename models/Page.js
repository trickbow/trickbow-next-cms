import mongoose from 'mongoose';

const {String, Boolean} = mongoose.Schema.Types;

const pageSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true
    },
    seo_title: {
        type: String,
        required: true,        
        unique: true
    },
    body: {
        type: String,
        required: true
    },
    order: {
        type: Number,
        default: 0
    },
    active: {
        type: Boolean,
        default: false
    },
    in_header: {
        type: Boolean,
        default: false
    },
    in_footer: {
        type: Boolean,
        default: false
    }
}, {
    timestamps: true
});

export default mongoose.models.Page || mongoose.model('Page', pageSchema);