import mongoose from 'mongoose';

const {String, Boolean} = mongoose.Schema.Types;

const settingsSchema = new mongoose.Schema({
    site_name: {
        type: String,
        default: ""
    },
    contact_email: {
        type: String,
        default: ""
    },
    facebook_link: {
        type: String,
        default: ""
    },
    twitter_link: {
        type: String,
        default: ""
    },
    logo_url: {
        type: String,
        default: ""
    },
    homepage_body: {
        type: String,
        default: ""
    },
    theme: {
        type: String,
        default: "Light"
    }
}, {
    timestamps: true
});

export default mongoose.models.Settings || mongoose.model('Settings', settingsSchema);