function catchErrors(error, displayError){
    let errorMsg;
    if(error.response){
        errorMsg = error.response.data;
        console.error("Error response", errorMsg);
        //For Cloudinary
        if(error.response.data.error){
            errorMsg = error.response.data.error.message;
            console.error("Error response", errorMsg);
        }
    }else if (error.reqest){
        errorMsg = error.reqest; 
        console.error("Error response", errorMsg);
    }else{
        errorMsg = error.message;
        console.error(errorMsg);
    }
    displayError(errorMsg);
}

export default catchErrors;