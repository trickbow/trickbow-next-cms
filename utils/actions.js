export const setSettings = settings =>({
    type: 'SET_SETTINGS',
    payload: settings
})

export const setUser = user =>({
    type: 'SET_USER',
    payload: user
})

export const setPages = pages =>({
    type: 'SET_PAGES',
    payload: pages
})