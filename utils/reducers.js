const defaultState = {};

export default (state=defaultState, action) => {
    switch (action.type){
        case 'SET_USER':
            return {...state, user: action.payload};
        case 'SET_SETTINGS':
            return {...state, settings: action.payload};
        case 'SET_PAGES':
            return {...state, pages: action.payload};
        default:
            return state; 
    }
}