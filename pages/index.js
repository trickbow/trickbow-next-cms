import {Button, Icon, Container} from 'semantic-ui-react';
import React from 'react';

function Home({user, settings}) {
  const isRoot = user && user.role === "root";
  const isAdmin = user && user.role === "admin";
  const isRootOrAdmin = isRoot || isAdmin;
  return (
  <>
  {

    isRootOrAdmin && (
      <Button
        color="teal"
        floated="right"
        href="/settings"
      >
        Settings
        <Icon
          name="settings"
          size="large"
          className="right"
        />
      </Button>
      )
    }
    <Container
      style={{clear: 'both'}}
        dangerouslySetInnerHTML={settings.homepage_body ? {__html: settings.homepage_body} : {__html: "Welcome to the home page!"}}
    />

  </>
  );
}

export default Home;
