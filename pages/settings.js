import React from 'react';
import {Form, Input, Image, Button, Message, Header, Icon, Radio} from 'semantic-ui-react';
import catchErrors from '../utils/catchErrors';
import dynamic from 'next/dynamic';
import baseUrl from '../utils/baseUrl';
import axios from 'axios';
import Router from 'next/router';

const MceEditor = dynamic(() => import('@tinymce/tinymce-react').then(mod => mod.Editor),{ ssr: false });

function Settings({user, settings}){
    const INITIAL_SETTINGS = settings;
    const [newSettings, setNewSettings] = React.useState(INITIAL_SETTINGS);
    const [logoPreview, setLogoPreview] = React.useState('');
    const [success, setSuccess] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [disabled, setDisabled] = React.useState(false);
    const [error, setError] = React.useState('');
    const isRoot = user && user.role === "root";
    const isAdmin = user && user.role === "admin";
    const isRootOrAdmin = isRoot || isAdmin;
    const CancelToken = axios.CancelToken;
    const source = CancelToken.source();

    const handleChange = (e) =>{
        const {name, value, files} = e.target;
        if(name === 'logo_url'){
            setNewSettings(prevState => ({...prevState, logo_url: files[0]}));
            setLogoPreview(window.URL.createObjectURL(files[0]));
        }else{
            setNewSettings(prevState => ({...prevState, [name]: value}));
        }
    }

    const handleThemeChange = (e, {value}) =>{
        setNewSettings(prevState => ({...prevState, theme: value}));
    }    

    const handleEditorChange = (content, editor) =>{
        setNewSettings(prevState => ({...prevState, homepage_body: content}));
    }

    const handleImageUpload = async () => {
        const data = new FormData();
        data.append('upload_preset', 'trickbowcms');
        data.append('cloud_name', 'trickbowcloud');
        data.append('file', newSettings.logo_url);
        const response = await axios.post(process.env.CLOUDINARY_URL, data);
        const mediaUrl = response.data.url;
        return mediaUrl;
      }

    const handleSubmit = async (e) =>{
        e.preventDefault();
        try{
            setError('');
            setLoading(true);
            let logo_url = "";
            if(newSettings.logo_url){
                logo_url = await handleImageUpload();
            }
            const url = `${baseUrl}/api/settings`;
            const payload = {...newSettings, logo_url};
            let response;
            //if the site is already setup use a put to change settings
            if(settings._id){
                response = await axios.put(url, payload,{cancelToken: source.token});
            }else{
            //if it's a new site, use post to enter new settings object into DB
                response = await axios.post(url, payload,{cancelToken: source.token});
            }
        }catch(error){
            catchErrors(error, setError);
            setError(error);
        }finally{
            setLoading(false);
            Router.reload();
        }
    }
    

    Router.events.on('routeChangeComplete', ()=>{
        if(settings._id){
            setNewSettings(settings);
        }
        source.cancel();
    });
 return(
     <>
        <Header as="h1">Settings</Header>        
        
            <Form loading={loading} error={Boolean(error)} success={success} onSubmit={handleSubmit}>
                <Message 
                success 
                icon="check"
                header="Success!"
                content="Your settings have been updated"
                />
                <Message 
                error 
                header="Oops!"
                content={error}
                />
                
                <Form.Field
                    control={Input}
                    name="site_name"
                    label="Site Name (This will show up in the browser tab)"
                    type="text"
                    value={newSettings.site_name}
                    onChange={handleChange}
                />

                <Form.Field
                    control={Input}
                    name="logo_url"
                    label="Logo"
                    content="Select Image"
                    type="file"
                    accept="image/*"
                    onChange={handleChange}
                />                

                <Image 
                    src={logoPreview ? logoPreview : settings.logo_url}
                    rounded 
                    centered
                    floated="left"
                    size="small"
                />
                
                <Form.Field
                    control={Input}
                    name="contact_email"
                    label="Contact Email (for the contact page)"
                    type="text"
                    value={newSettings.contact_email}
                    onChange={handleChange}
                />
                
                <Form.Field
                    control={Input}
                    name="facebook_link"
                    label="Facebook Link (can be blank)"
                    type="text"
                    value={newSettings.facebook_link}
                    onChange={handleChange}
                />
                
                <Form.Field
                    control={Input}
                    name="twitter_link"
                    label="Twitter Link (can be blank)"
                    type="text"
                    value={newSettings.twitter_link}
                    onChange={handleChange}
                />
                                
                <Header as="h2" style={{clear: 'both'}}>Homepage Content</Header>

                <MceEditor
                    name="homepage_body"
                    value={newSettings.homepage_body}
                    apiKey={process.env.TINYMCE_API_KEY}
                    init={{
                        height: 500,
                        menubar: true,
                        images_upload_handler: function (blobInfo, success, failure) {
                            let xhr, formData;
                            xhr = new XMLHttpRequest();
                            xhr.withCredentials = false;
                            //restricted it to image only using resource_type = image in url, you can set it to auto for all types 
                            xhr.open('POST', process.env.CLOUDINARY_URL);
                            xhr.onload = function () {
                                if (xhr.readyState == 4 && xhr.status == 200) {
                                    let response = JSON.parse(xhr.responseText);
                                    var url = response.secure_url; //get the url 
                                    var json = {location: url}; //set it in the format tinyMCE wants it
                                    success(json.location);
                                }                                
                                if (xhr.status !== 200) {
                                    failure('HTTP Error: ' + xhr.status);
                                    return;
                                }
                            };
                            formData = new FormData();
                            formData.append('file', blobInfo.blob(), blobInfo.filename());
                            formData.append('upload_preset', 'trickbowcms');
                            formData.append('cloud_name', 'trickbowcloud');
                            xhr.send(formData);
                        },
                        plugins: [
                        'advlist autolink lists link image charmap print preview anchor',
                        'searchreplace visualblocks code fullscreen',
                        'insertdatetime media table paste code help wordcount'
                        ],
                        toolbar:
                        'undo redo | formatselect | bold italic backcolor | \
                        alignleft aligncenter alignright alignjustify | \
                        bullist numlist outdent indent | removeformat | help'
                    }}
                    onEditorChange={handleEditorChange}
                />                
                <Header as="h2">Theme</Header>
                
                <Form.Field
                    name="theme"
                    control={Radio}
                    checked={newSettings.theme === "Light" || newSettings.theme === "" || newSettings.theme === undefined}
                    label="Light"
                    value="Light"
                    onChange={handleThemeChange}
                />
                <Form.Field
                    name="theme"
                    control={Radio}
                    checked={newSettings.theme === "Grey"}
                    label="Grey"
                    value="Grey"
                    onChange={handleThemeChange}
                />
                <Form.Field
                    name="theme"
                    control={Radio}
                    checked={newSettings.theme === "Dark"}
                    label="Dark"
                    value="Dark"
                    onChange={handleThemeChange}
                />
                <Form.Field
                    name="theme"
                    control={Radio}
                    checked={newSettings.theme === "Blue"}
                    label="Blue"
                    value="Blue"
                    onChange={handleThemeChange}
                />
                <p>

                    <Button 
                        type="submit"
                        color="teal"
                        disabled={disabled || loading}
                    >
                        <Icon 
                            name="save"
                            size="large"
                        />
                        Save Settings
                    </Button>
                </p>
        </Form>
     </>
 );
}

export default Settings;