import App from "next/app";
import React from 'react';
import Layout from "../components/_App/Layout";
import {parseCookies, destroyCookie} from 'nookies';
import {redirectUser} from '../utils/auth';
import baseUrl from '../utils/baseUrl';
import axios from 'axios';
import Router from "next/router";
import reduxStore from '../utils/reduxStore';
import {setSettings, setUser, setPages} from '../utils/actions';
import {Provider} from 'react-redux';
const CancelToken = axios.CancelToken;
const source = CancelToken.source();

function MyApp (props){
  React.useEffect(()=>{
    window.addEventListener('storage', syncLogout);
    return () =>{
      source.cancel();
    }
  }, [])

  const syncLogout = (e) =>{
    if(e.key === "logout"){
      Router.push('/login');
    }
  }
    const { Component, pageProps } = props;    

    const store = reduxStore();
    
    store.dispatch(setSettings(props.pageProps.settings));
    store.dispatch(setUser(props.pageProps.user));
    store.dispatch(setPages(props.pageProps.pages));

  return (
    <Provider store={store}>
      <Layout {...props.pageProps}>
        <Component {...props.pageProps} />
      </Layout>
    </Provider>
  );
}

MyApp.getInitialProps = async ({ Component, ctx}) => {
  const { token } = parseCookies(ctx);
  let pageProps = {};
  if(Component.getInitialProps){
    pageProps = await Component.getInitialProps(ctx);
  }
  if(!token){
    const isProtectedRoute = ctx.pathname === '/account' || ctx.pathname === '/create' || ctx.pathname === '/pages' || ctx.pathname === '/settings';
    if(isProtectedRoute){
      redirectUser(ctx, '/login');
    }
  }else{
    try{
      const url= `${baseUrl}/api/account`;
      const payload = { headers: { Authorization: token }}
      const response = await axios.get(url, payload, {cancelToken: source.token});
      const user = response.data;
      const isRoot = user.role === 'root';
      const isAdmin = user.role === 'admin';
      //if standard user, redirect from /create
      const isStandardUser = !(isRoot || isAdmin) && ctx.pathname === '/create';
      if(isStandardUser){
        redirectUser(ctx, '/');
      }

      pageProps.user = user;
    }catch(error){
      console.error("Error getting current user", error);
      // Throw out invalid token
      destroyCookie(ctx, "token");
      // Redirect to login page
      redirectUser(ctx, '/login');
    }
  }
   //get a list of pages and put them in props
   try{
    const url = `${baseUrl}/api/pages`;
    const response = await axios.get(url, {cancelToken: source.token});
    pageProps.pages = response.data;
  }catch(error){
    console.error("Error getting pages",error);
    pageProps.pages = {};
  }
  
  //get site settings
  try{
    const url = `${baseUrl}/api/settings`;
    const response = await axios.get(url,{cancelToken: source.token});
    pageProps.settings = response.data;
  }catch(error){
    console.error("Error getting settings",error);
    pageProps.settings = {};
  }

  return {pageProps};
}


export default MyApp;
