import nodemailer from 'nodemailer';

module.exports = async (req, res) => {

    const transport = {
        host: 'smtpout.secureserver.net',
        port: 465,
        secureConnection: true,
        secure: true,
        auth:{
            user: process.env.EMAIL_ADDRESS,
            pass: process.env.EMAIL_PASSWORD
        }
    }

    const transporter = nodemailer.createTransport(transport);

    transporter.verify((error, success) => {
        if(error){
            console.error(error);
        }
        else{
            console.log("Server is ready to take messages.")
        }
    });

    const {name, email, body, contact_email} = req.body;
    const content = `Name: ${name} \n Email: ${email} \n Message: ${body}`
    const mail = {
        from: process.env.EMAIL_ADDRESS,
        to: contact_email,
        subject: 'New CMS contact form submission',
        text: content
    }

    await transporter.sendMail(mail, (err, data) =>{
        if (err) {
                console.error(err);
                res.json({
                msg: 'fail'
            })
        } else {
                res.json({
                msg: 'success'
            })
        }
    })
}