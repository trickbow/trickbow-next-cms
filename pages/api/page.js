import Page from '../../models/Page';
import connectDb from "../../utils/connectDb";

connectDb();

module.exports = async (req, res) =>{
    switch(req.method){
        case "GET":
            handleGetRequest(req, res);
            break;
        case "POST":
            handlePostRequest(req, res);
            break;
        case "PUT":
            handlePutRequest(req, res);
            break;
        case "DELETE":
            handleDeleteRequest(req, res);
            break;
        default:
            res.status(405).send(`Method ${req.method} not allowed.`);
            break;
    }
}

const handleGetRequest = async (req, res) =>{
    try{
        const { page_name } = req.query;
        const page = await Page.findOne({"seo_title": page_name});
        res.status(200).json(page);
    }catch(error){
        res.status(500).send("Sorry, server borked. Page a no-go")
    }
}

const handlePutRequest = async (req, res) =>{
    const {_id, title, seo_title, body, order, active, in_header, in_footer} = req.body;
    try{
        if(!title || !body){
            return res.status(422).send("A new page needs a title and body");
        }
        await Page.findOneAndUpdate(
            {_id},
            {title, seo_title, body, order, active, in_header, in_footer}
        );
        res.status(203).send("Page was successfully updated.");
    }catch(error){
        console.error(error);
        res.status(500).send("Sorry - server borked. Error creating page")
    }
}

const handlePostRequest = async (req, res) =>{
    const {title, seo_title, body, order, active, in_header, in_footer} = req.body;
    try{
        if(!title || !body){
            return res.status(422).send("A new page needs a title and body");
        }
        const page = await new Page({
            title,
            seo_title,
            body,
            order,
            active,
            in_header,
            in_footer
        }).save();
        res.status(201).json(page);
    }catch(error){
        console.error(error);
        res.status(500).send("Sorry - server borked. Error creating page")
    }
}

const handleDeleteRequest = async (req, res) =>{
    try{
    const { _id } = req.query;
    // Delete page by id
    await Page.findOneAndDelete({ _id });
    res.status(204).json({});
    } catch(error){
        res.status(500).send('Sorry, server broked. Can\'t delete page');
    }
}