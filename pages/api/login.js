import connectDb from '../../utils/connectDb';
import User from '../../models/User';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

connectDb();

module.exports =  async (req, res) => {
    const {email, password} = req.body;
    try{
        //check if user exists
        const user = await User.findOne({email}).select('+password');

        //if not, return error
        if(!user){
            return res.status(404).send("No user exists with that email")
        }
        //check if password matches db
        const passwordsMatch = await bcrypt.compare(password, user.password);
        if(passwordsMatch){
            //generate token
            const token = jwt.sign({userId: user._id}, process.env.JWT_SECRET, {expiresIn: '7d'});            
            //send token to client
            res.status(200).json(token);
        }else{
            res.status(401).send("Password doesn't match")
        }
    }catch(error){
        console.error(error);
        res.status(500).send("Error logging in user");
    }
}