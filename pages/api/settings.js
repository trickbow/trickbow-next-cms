import Settings from '../../models/Settings';
import connectDb from "../../utils/connectDb";

connectDb();

module.exports = async (req, res) =>{
    switch(req.method){
        case "GET":
            handleGetRequest(req, res);
            break;
        case "PUT":
            handlePutRequest(req, res);
            break;
        case "POST":
            handlePostRequest(req, res);
            break;
        default:
            res.status(405).send(`Method ${req.method} not allowed.`);
            break;
    }
}

const handleGetRequest = async (req, res) =>{
    try{
    const settings = await Settings.find();
        res.status(200).json(settings[0]);
    }catch(error){
        console.error(error);
        res.status(405).send("Error getting settings");
    }
}

const handlePostRequest = async(req, res) =>{
    const {site_name, contact_email, facebook_link, twitter_link, logo_url, homepage_body, theme} = req.body;
    try{
        // if(!site_name || !logo_url || !homepage_body){
        //     return res.status(422).send("The settings need a page name, logo, and body");
        // }
        const settings = await new Settings({
            site_name,
            contact_email,
            facebook_link,
            twitter_link,
            logo_url,
            homepage_body,
            theme
        }).save();
        res.status(201).json(settings);
    }catch(error){
        console.error(error);
        res.status(500).send("Sorry - server borked. Error updating settings")
    }
}

const handlePutRequest = async(req, res) =>{
    const {_id, contact_email, facebook_link, twitter_link, site_name, logo_url, homepage_body, theme} = req.body;
    try{
        // if(!site_name || !logo_url || !homepage_body){
        //     return res.status(422).send("The settings need a page name, logo, and body");
        // }
        await Settings.findOneAndUpdate(
            {_id},
            {site_name, contact_email, facebook_link, twitter_link, logo_url, homepage_body, theme}
        );
        res.status(203).send("Settings were successfully updated");
    }catch(error){
        console.error(error);
        res.status(500).send("Sorry - server borked. Error updating settings")
    }
}