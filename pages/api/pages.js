import connectDb from '../../utils/connectDb';
import Page from '../../models/Page';

connectDb();

module.exports =  async (req, res) => {
    const pages = await Page.find({}).select('_id title seo_title active in_header in_footer order');
    res.status(200).json(pages);
}