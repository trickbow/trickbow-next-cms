import connectDb from '../../utils/connectDb';
import User from '../../models/User';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import isEmail from 'validator/lib/isEmail';
import isLength from 'validator/lib/isLength';

connectDb();

module.exports = async (req, res) => {
    const {name, email, password} = req.body
    try{
        //validate name/email/password
        if(!isLength(name, {min: 2, max: 30})){
            return res.status(422).send("Name must be 2-30 characters long");
        }
        if(!isLength(password, {min: 7})){
            return res.status(422).send("Password must be at least 7 characters long");
        }
        if(!isEmail(email)){
            return res.status(422).send("The email address is invalid");
        }

        const user = await User.findOne({email});
        if(user){
            return res.status(422).send(`User already exists with email ${email}`);
        }
        //hash the password before sending to DB
        const hash = await bcrypt.hash(password, 10);
        const newUser = await new User({
            name,
            email,
            password: hash
        }).save();
        //create a secure, unique token for new user
        const token = jwt.sign({ userId: newUser._id }, process.env.JWT_SECRET, { expiresIn: '7d' });
        res.status(201).json(token);

    }catch(error){
        console.error(error);
        res.status(500).send("Error creating user. Please try again later.")
    }
}