import {Form, Input, Button, Message, Header, Icon, Checkbox, Table, Label} from 'semantic-ui-react';
import React from 'react';
import axios from 'axios';
import baseUrl from '../utils/baseUrl';
import catchErrors from '../utils/catchErrors';
import dynamic from 'next/dynamic';
import { useRouter } from 'next/router';

const INITIAL_PAGE = {
  title: "",
  seo_title: "",
  body: "",
  active: false,
  in_header: false,
  in_footer: false
}

const MceEditor = dynamic(() => import('@tinymce/tinymce-react').then(mod => mod.Editor),{ ssr: false });

function CreatePage() {
  const [page, setPage] = React.useState(INITIAL_PAGE);
  const [active, setActive] = React.useState(false);
  const [in_header, setInHeader] = React.useState(false);
  const [in_footer, setInFooter] = React.useState(false);
  const [success, setSuccess] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [disabled, setDisabled] = React.useState(true);
  const [error, setError] = React.useState('');

  React.useEffect(() => {
    let values = [];
    Object.entries(page).forEach(el => {
      if(el[0] === "title" || el[0] === "body") {
        values.push(el[1]);
      }
    })
    const isPage = values.every(el => Boolean(el));
    isPage ? setDisabled(false) : setDisabled(true);
  }, [page]);

  const router = useRouter();

  const handleChange = (e) =>{
    const {name, value} = e.target;
    if(name === "title"){
      const seoTitle = value.replace(new RegExp(' ', 'g'), '-').toLowerCase().trim();
      setPage(prevState => ({...prevState, ['seo_title']: encodeURI(seoTitle)}));
    }
    setPage(prevState => ({...prevState, [name]: value}));
  }
  
  const handleEditorChange = (content, editor) =>{
    setPage(prevState => ({...prevState, ['body']: content}));
  }

  const handleSubmit = async (e) =>{
    try{
      e.preventDefault();
      setError('');
      setLoading(true);
      const url = `${baseUrl}/api/page`;
      const payload = {...page};
      const response = await axios.post(url, payload);
      setPage(INITIAL_PAGE);
      setSuccess(true);
    }catch(error){
      catchErrors(error, setError);
    } finally{
      setLoading(false);
      router.push(`/page?page_name=${page.seo_title}`);
    }
  }

  const handleToggleCheckbox = (e, data) =>{
      switch(data.name){
        case "active":
          setActive(prevState => !prevState);
          break;
        case "in_header":
          setInHeader(prevState => !prevState);
          break;
        case "in_footer":
          setInFooter(prevState => !prevState);
          break;
      }
      setPage(prevState => ({...prevState, [data.name]: data.checked}));
  }

  return (
  <>
    <Header as="h2" block>
      <Icon name= "add" color="orange" />
      Create New Page
    </Header>
    <Form loading={loading} error={Boolean(error)} success={success} onSubmit={handleSubmit}>
      <Message 
        success 
        icon="check"
        header="Success!"
        content="Your page has been posted"
      />
      <Message 
        error 
        header="Oops!"
        content={error}
      />
      <Form.Group widths="equal">
        <Form.Field
          control={Input}
          name="title"
          label="Title"
          placeholder="Title"
          value={page.title}
          onChange={handleChange}
        />
        <Form.Field
          style={{width: '25%'}}
          control={Input}
          type="number"
          step="1"
          min="0"
          name="order"
          label="Order"
          placeholder="0"
          initialValue={page.order}
          onChange={handleChange}
        />
      </Form.Group>
      <MceEditor
        name="body"
        initialValue="<p>Insert page text.</p>"
        apiKey={process.env.TINYMCE_API_KEY}
        init={{
          height: 500,
          menubar: true,
          images_upload_handler: function (blobInfo, success, failure) {
            let xhr, formData;
            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;
            //restricted it to image only using resource_type = image in url, you can set it to auto for all types 
            xhr.open('POST', process.env.CLOUDINARY_URL);
            xhr.onload = function () {
                if (xhr.readyState == 4 && xhr.status == 200) {
                    let response = JSON.parse(xhr.responseText);
                    var url = response.secure_url; //get the url 
                    var json = {location: url}; //set it in the format tinyMCE wants it
                    success(json.location);
                }                                
                if (xhr.status !== 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }
            };
            formData = new FormData();
            formData.append('file', blobInfo.blob(), blobInfo.filename());
            formData.append('upload_preset', 'trickbowcms');
            formData.append('cloud_name', 'trickbowcloud');
            xhr.send(formData);
          },
          plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste code help wordcount'
          ],
          toolbar:
            'undo redo | formatselect | bold italic backcolor | \
            alignleft aligncenter alignright alignjustify | \
            bullist numlist outdent indent | removeformat | help'
        }}
        onEditorChange={handleEditorChange}
      />
      <Table>
        <Table.Body>
          <Table.Row>
          <Table.Cell collapsing>
            <Checkbox name="active" checked={active} toggle onChange={handleToggleCheckbox} />
            <Label>Active</Label>
          </Table.Cell>
          <Table.Cell collapsing>
            <Checkbox name="in_header" checked={in_header} toggle onChange={handleToggleCheckbox} />
            <Label>In Header Menu</Label>
          </Table.Cell>
          <Table.Cell collapsing>
            <Checkbox name="in_footer" checked={in_footer} toggle onChange={handleToggleCheckbox} />
            <Label>In Footer Menu</Label>
          </Table.Cell>
        </Table.Row>
        </Table.Body>
      </Table>
      <Form.Field 
        control={Button}
        color="blue"
        icon="pencil alternate"
        content="Submit"
        type="submit"
        disabled={loading || Boolean(disabled)}
      />
    </Form>
  </>
  );
}

export default CreatePage;
