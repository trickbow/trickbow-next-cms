import React from 'react';
import {Header, Container, Message, Button, Modal, Table, Form, Label, Checkbox, Input, Icon} from 'semantic-ui-react';
import baseUrl from '../utils/baseUrl';
import axios from 'axios';
import formatDate from '../utils/formatDate';
import Router, { useRouter } from 'next/router';
import dynamic from 'next/dynamic';
import catchErrors from '../utils/catchErrors';
import {renderToStaticMarkup} from 'react-dom/server';
import pdf from 'html-pdf';
const CancelToken = axios.CancelToken;
const source = CancelToken.source();

const MceEditor = dynamic(() => import('@tinymce/tinymce-react').then(mod => mod.Editor),{ ssr: false });

function Page({ user, page }){
  const INITIAL_PAGE = page;
  const [pageUpdates, setPageUpdates] = React.useState(INITIAL_PAGE);
  const [editModal, setEditModal] = React.useState(false);
  const [deleteModal, setDeleteModal] = React.useState(false);
  const [active, setActive] = React.useState(page.active);
  const [in_header, setInHeader] = React.useState(page.in_header);
  const [in_footer, setInFooter] = React.useState(page.in_footer);
  const [success, setSuccess] = React.useState(false);
  const [loading, setLoading] = React.useState(false);
  const [disabled, setDisabled] = React.useState(false);
  const [error, setError] = React.useState('');
  const isRoot = user && user.role === "root";
  const isAdmin = user && user.role === "admin";
  const isRootOrAdmin = isRoot || isAdmin;
  const router = useRouter();

  React.useEffect(() => {
    let values = [];
    Object.entries(pageUpdates).forEach(el => {
      if(el[0] === "title" || el[0] === "body") {
        values.push(el[1]);
      }
    })
    const isPage = values.every(el => Boolean(el));
    isPage ? setDisabled(false) : setDisabled(true);
  }, [pageUpdates]);

  Router.events.on('routeChangeComplete', ()=>{
    setPageUpdates(page);
  });

  const handleDelete = async () =>{
    const url = `${baseUrl}/api/page`;
    const payload = {params: {_id: page._id}};
    await axios.delete(url, payload);
    router.push('/');
  }

  const handleChange = (e) =>{
    const {name, value} = e.target;
    if(name === "title"){
      const seoTitle = value.replace(/\s/g, '-').toLowerCase().trim();
      setPageUpdates(prevState => ({...prevState, ['seo_title']: encodeURI(seoTitle)}));
    }
    if(name === "order"){
      setPageUpdates(prevState => ({...prevState, [name]: Number(value)}));
    }
    else{
      setPageUpdates(prevState => ({...prevState, [name]: value}));
    }
  }

  const handleEditorChange = (content, editor) =>{
    setPageUpdates(prevState => ({...prevState, ['body']: content}));
  }

  const handleSubmit = async (e) =>{
    try{
      e.preventDefault();
      setError('');
      setLoading(true);
      const url = `${baseUrl}/api/page`;
      const payload = {...pageUpdates, cancelToken: source.token};
      const response = await axios.put(url, payload);
      setSuccess(true);
    }catch(error){
      if(axios.isCancel(error)){
        catchErrors("Canceled DB call.", setError);
      }else{
        catchErrors(error, setError);
      }
    } finally{
      //setLoading(false);
      router.reload();
    }
  }

  const handleToggleCheckbox = (e, data) =>{
      switch(data.name){
        case "active":
          setActive(prevState => !prevState);
          break;
        case "in_header":
          setInHeader(prevState => !prevState);
          break;
        case "in_footer":
          setInFooter(prevState => !prevState);
          break;
      }
      setPageUpdates(prevState => ({...prevState, [data.name]: data.checked}));
  }

  // const convertToPdf = () => {
  //   const element = document.getElementById('main-container');
  //   const opt = {
  //     margin:       1,
  //     filename:     `drew-nusser-me.pdf`,
  //     image:        { type: 'jpeg', quality: 0.98 },
  //     html2canvas:  { scale: 2 },
  //     jsPDF:        { unit: 'in', format: 'letter', orientation: 'portrait' }
  //   }; 
  //   // New Promise-based usage:
  //   html2pdf().from(element).set(opt).save();
  // }

  const printDocument = () =>{
    window.print();
  }

    return (
        <>
        <a id="printButton" style={{float: 'right'}} onClick={printDocument}>
          <Icon
            size="large"
            name="print"
          />  
        </a>
        {/* <a id="pdfButton" style={{float: 'right'}} onClick={convertToPdf}>
          <Icon
            size="large"
            name="file pdf"
          />  
        </a> */}
        <Header as="h1">{page.title}</Header>
        <Container
          dangerouslySetInnerHTML={{__html: page.body}}
        />
        {isRootOrAdmin && (
      <>
        <Message>Last updated: {formatDate(page.updatedAt)}</Message>
        <Button
          icon="pencil alternate"
          color="teal"
          content="Edit Page"
          onClick={()=>setEditModal(!editModal)}
        />
        <Modal open={editModal} id="CoolModal" >
          <Modal.Header>Edit Page</Modal.Header>
          <Form loading={loading} error={Boolean(error)} success={success} onSubmit={handleSubmit}>
            <Message 
              success 
              icon="check"
              header="Success!"
              content="Your page has been updated"
            />
            <Message 
              error 
              header="Oops!"
              content={error}
            />
            <Form.Group widths="equal" style={{padding: '1em'}}>
              <Form.Field
                control={Input}
                name="title"
                label="Title"
                placeholder="Title"
                value={pageUpdates.title}
                onChange={handleChange}
              />
              <Form.Field
                style={{width: '25%'}}
                control={Input}
                name="order"
                label="Order"
                placeholder="Order"
                min="1"
                step="1"
                type="number"
                value={pageUpdates.order}
                onChange={handleChange}
              />
            </Form.Group>
            <MceEditor
              name="body"
              value={pageUpdates.body}
              apiKey={process.env.TINYMCE_API_KEY}
              init={{
                height: 500,
                menubar: true,
                images_upload_handler: function (blobInfo, success, failure) {
                  let xhr, formData;
                  xhr = new XMLHttpRequest();
                  xhr.withCredentials = false;
                  //restricted it to image only using resource_type = image in url, you can set it to auto for all types 
                  xhr.open('POST', process.env.CLOUDINARY_URL);
                  xhr.onload = function () {
                      if (xhr.readyState == 4 && xhr.status == 200) {
                          let response = JSON.parse(xhr.responseText);
                          var url = response.secure_url; //get the url 
                          var json = {location: url}; //set it in the format tinyMCE wants it
                          success(json.location);
                      }                                
                      if (xhr.status !== 200) {
                          failure('HTTP Error: ' + xhr.status);
                          return;
                      }
                  };
                  formData = new FormData();
                  formData.append('file', blobInfo.blob(), blobInfo.filename());
                  formData.append('upload_preset', 'trickbowcms');
                  formData.append('cloud_name', 'trickbowcloud');
                  xhr.send(formData);
                },
                plugins: [
                  'advlist autolink lists link image charmap print preview anchor',
                  'searchreplace visualblocks code fullscreen',
                  'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                  'undo redo | formatselect | bold italic backcolor | \
                  alignleft aligncenter alignright alignjustify | \
                  bullist numlist outdent indent | removeformat | help'
              }}
              onEditorChange={handleEditorChange}
            />
            <Table>
              <Table.Body>
                <Table.Row>
                <Table.Cell collapsing>
                  <Checkbox label="Active" name="active" checked={active} toggle onChange={handleToggleCheckbox} />
                </Table.Cell>
                <Table.Cell collapsing>
                  <Checkbox label="In Header Menu" name="in_header" checked={in_header} toggle onChange={handleToggleCheckbox} />
                </Table.Cell>
                <Table.Cell collapsing>
                  <Checkbox label="In Footer Menu" name="in_footer" checked={in_footer} toggle onChange={handleToggleCheckbox} />
                </Table.Cell>
              </Table.Row>
              </Table.Body>
            </Table>
          </Form>

          <Modal.Actions>
            <Button 
              content="Cancel" 
              onClick={()=>setEditModal(!editModal)} 
              disabled={Boolean(disabled) || loading}
            />
            <Button 
              content="Save" 
              color="teal"
              icon="save"
              onClick={handleSubmit}
              disabled={Boolean(disabled) || loading}
            />
          </Modal.Actions>
        </Modal>
        <Button
          icon="trash alternate outline"
          color="red"
          content="Delete Page"
          onClick={()=>setDeleteModal(!deleteModal)}
        />
        <Modal open={deleteModal} dimmer="blurring" id="CoolModal">
          <Modal.Header>Confirm Delete</Modal.Header>
          <Modal.Content><p>Are you sure you want to delete this page? There's no going back.</p></Modal.Content>
          <Modal.Actions>
            <Button 
              content="Cancel" 
              onClick={()=>setDeleteModal(!deleteModal)}            
              disabled={Boolean(disabled) || loading}
             />
            <Button 
              content="Delete" 
              color="red"
              labelPosition="right"
              icon="trash"
              onClick={handleDelete}
              negative
            />
          </Modal.Actions>
        </Modal>
        </>)
        }
        </>
    );
}

Page.getInitialProps = async ( {query:{page_name}} ) => {
    const url = `${baseUrl}/api/page`;
    const payload = { params: {page_name}, cancelToken: source.token};
    const response = await axios.get(url, payload);
  return {page: response.data};
}

export default Page;