import {Table, Icon, Button} from 'semantic-ui-react';
import Link from 'next/link';

function Pages({pages}){
    return(
        <>
            <Button color="teal" style={{float: "right", margin: "1em 0"}} href="/create">
              <Icon
                name="add square"
                size="large"
              />
              Create Page
            </Button>
            <Table>
                <Table.Body>
                    <Table.Row>
                        <Table.HeaderCell>
                            Title
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                            Active
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                            In Header
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                            In Footer
                        </Table.HeaderCell>
                        <Table.HeaderCell>
                            Order
                        </Table.HeaderCell>
                    </Table.Row>
                {
                pages.sort((a,b) => a.order > b.order).map((page) =>{
                    return (
                        <Table.Row key={page._id}>
                            <Table.Cell>
                                <div>
                                    <Link href={`/page?page_name=${page.seo_title}`} >
                                        <a>{page.title}</a>
                                    </Link>
                                </div>
                            </Table.Cell>
                            <Table.Cell>
                                <div>
                                    {page.active.toString()}
                                </div>
                            </Table.Cell>
                            <Table.Cell>
                                <div>
                                    {page.in_header.toString()}
                                </div>
                            </Table.Cell>
                            <Table.Cell>
                                <div>
                                {page.in_footer.toString()}
                                </div>
                            </Table.Cell>
                            <Table.Cell>
                                <div>
                                {page.order}
                                </div>  
                            </Table.Cell>
                        </Table.Row>
                    );
                })}
                </Table.Body>
            </Table>
        </>
    );
}

export default Pages;