import {Form, Input, TextArea, Button, Message, Header, Icon, Checkbox, Table, Label, CommentAction} from 'semantic-ui-react';
import React from 'react';
import catchErrors from '../utils/catchErrors';
import Router from 'next/router';
import axios from 'axios';
import baseUrl from '../utils/baseUrl';
import {connect} from 'react-redux';

function Contact(props){
    const [message, setMessage] = React.useState({name: "", email: "", body: ""});
    const [success, setSuccess] = React.useState(false);
    const [loading, setLoading] = React.useState(false);
    const [disabled, setDisabled] = React.useState(true);
    const [error, setError] = React.useState('');
    const CancelToken = axios.CancelToken;
    let source = CancelToken.source();

    React.useEffect(() => {
        const isMessage = Object.values(message).every(el => Boolean(el));
        isMessage ? setDisabled(false) : setDisabled(true);   
        return () =>{
            source.cancel();
        }
    }, [message]);

    const handleChange = (e) =>{
        const {name, value} = e.target;
        setMessage(prevState => ({...prevState, [name]: value}));
    }

    const handleSubmit = async (e) =>{
        e.preventDefault();
        try{
            setError('');
            setLoading(true);
            const url = `${baseUrl}/api/sendemail`;
            const payload = {...message, contact_email: props.settings.contact_email};
            const response = await axios.post(url, payload, {cancelToken: source.token});
            setSuccess(true);
          }catch(error){
            catchErrors(error, setError);
          }finally{
            setLoading(false);
            setMessage(prevState => ({...prevState, body: ""}));
          }
    }
    
    Router.events.on('routeChangeComplete', ()=>{
        if(props.user){            
            const INITIAL_MESSAGE = {
                name: props.user.name,
                email: props.user.email,
                body: ""
            }
            setMessage(INITIAL_MESSAGE);
        }
    });

    return(
        <>
            <Header as="h1">Contact</Header>
            <Form loading={loading} error={Boolean(error)} success={success} onSubmit={handleSubmit} style={{maxWidth:"500px"}}>
                <Message 
                    success 
                    icon="check"
                    header="Success!"
                    content="Your email was sent. Please give us a few days to get back to you."
                />
                <Message 
                    error 
                    header="Oops!"
                    content={error}
                />
                <Form.Field
                    control={Input}
                    name="name"
                    icon="user"
                    iconPosition="left"
                    label="Name"
                    placeholder="Name"
                    value={message.name}
                    onChange={handleChange}
                >
                </Form.Field>
                <Form.Field
                    control={Input}
                    icon="envelope"
                    iconPosition="left"
                    name="email"
                    type="email"
                    label="Email"
                    placeholder="Email"
                    value={message.email}
                    onChange={handleChange}
                >
                </Form.Field>
                <Form.Field
                    control={TextArea}
                    rows={5}
                    name="body"
                    label="Body"
                    placeholder="Body"
                    value={message.body}
                    onChange={handleChange}
                >
                </Form.Field>
                <Button
                    color="teal"
                    disabled={disabled || loading}
                    type="submit"
                >
                    Send Message
                    <Icon 
                        name="send"
                        size="large"
                        className="right"
                    />
                </Button>
            </Form>
        </>
    );
}

const mapStateToProps = (state) => {
    return {
      user: state.user,
      settings: state.settings
    }
  }
  
  export default connect(mapStateToProps)(Contact);