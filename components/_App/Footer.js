import {Menu, Container, Icon, Image} from 'semantic-ui-react';
import Link from 'next/link';
import Router, { useRouter } from 'next/router';
import { connect } from 'react-redux';

function Footer(props){
    const router = useRouter();
  function isActive(route){
    return route === router.pathname;
  }
  return (
      <footer>
          <Menu
            id="footer-menu"
            fluid
            style={{maxWidth: '1200px', margin: '0 auto'}}
          >
            <Container floated="left">
              <Menu.Item>
                <div id="copyright">
                    &copy; {new Date().getFullYear()} {props.settings.site_name ? props.settings.site_name : 'Trick-Bow Productions'}<br />
                </div>
              </Menu.Item>
              {props.settings.contact_email && 
              <Link href="/contact">
                <Menu.Item active={isActive('/contact')}>
                    Contact
                </Menu.Item>
              </Link>
              }
              {
                // Get all the page links that belong in the header menu.
                props.pages.sort((a,b) => a.order > b.order).map((page) =>{
                return (page.active && page.in_footer) &&
                    <Link key={page._id} href={`/page?page_name=${page.seo_title}`}>
                      <Menu.Item active={isActive(`/page?page_name=${page.seo_title}`)}>
                        {page.title}
                      </Menu.Item>
                    </Link>
                })
                }
              </Container>
              <Container style={{justifyContent: 'flex-end'}}> 
              
              {props.settings.facebook_link &&
                <a target="_blank" href={props.settings.facebook_link}>
                  <Menu.Item>
                    <Icon 
                      name="facebook"
                      size="large"
                    />
                  </Menu.Item>
                </a>  
                }    
                {props.settings.twitter_link &&        
                <a target="_blank" href={props.settings.twitter_link}>
                  <Menu.Item>
                    <Icon 
                      name="twitter"
                      size="large"
                    />
                  </Menu.Item>
                </a>
                }
            </Container>
          </Menu>
      </footer>
  )
}

const mapStateToProps = (state) => {
  return {
    pages: state.pages,
    settings: state.settings
  }
}

export default connect(mapStateToProps)(Footer);