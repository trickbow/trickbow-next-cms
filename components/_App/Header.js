import {Menu, Container, Icon, Image, Button} from 'semantic-ui-react';
import Link from 'next/link';
import Router, { useRouter } from 'next/router';
import NProgress from 'nprogress';
import { handleLogout } from '../../utils/auth';
import React from 'react';
import { connect } from 'react-redux';

Router.onRouteChangeStart = () => {NProgress.start()};
Router.onRouteChangeComplete = () => {NProgress.done()};
Router.onRouteChangeError = () => {NProgress.done()};

function Header(props) {
  const [pageTop, setPageTop] = React.useState(true);
  const [menuOpen, setMenuOpen] = React.useState(false);
  const router = useRouter();
  const isRoot = props.user && props.user.role === "root";
  const isAdmin = props.user && props.user.role === "admin";
  const isRootOrAdmin = isRoot || isAdmin;

  const orderedPages = props.pages.sort((a,b) => a.order - b.order);


  React.useEffect((e)=>{
    window.addEventListener('scroll', function(){
      let scrollPosition = window.pageYOffset;
      if(scrollPosition >= 180) {
        pageTop && setPageTop(false);
      }else{
        !pageTop && setPageTop(true);
      }
    });
  })

  function isActive(route){
    return route === router.pathname;
  }

  const handleMenuButtonClick = (e) =>{
    document.getElementById('menu').classList.toggle('show');
    document.getElementById('overlay').classList.toggle('show');
    setMenuOpen(!menuOpen);
  }

  const hideMenu = () =>{
    document.getElementById('menu').classList.remove('show');
    document.getElementById('overlay').classList.remove('show');
    setMenuOpen(false);
  }

  return (
    <>    
    <div id="overlay" onClick={handleMenuButtonClick}></div>
    <header>
      <Menu 
        id="topmenu"
        style={{maxWidth: '1200px'}}
        >
      <Link href="/">
        <Menu.Item style={{border: "none"}}>
          <Image
            size={pageTop ? "small" : "mini"}
            src={props.settings.logo_url ? props.settings.logo_url : "/static/images/DNLogo.png"}
            style={{marginRight: '1em'}}
          />
        </Menu.Item>
      </Link>
      <Button id="menu-button" floated="right" onClick={handleMenuButtonClick}>
        <Menu.Item>
        <Icon
            name={menuOpen ? "close" : "bars"}
            size="large"
          />
        </Menu.Item>
      </Button>
      </Menu>
      <Menu 
        fluid 
        id="menu"
        stackable
        >
        <Container text>          
          <Link href="/">
            <Menu.Item onClick={hideMenu} header active={isActive('/')}>
              Home
            </Menu.Item>
          </Link>

          {}
        {            
        // Get all the page links that belong in the header menu, sort them in a new array.
          orderedPages.map((page) =>{
            return (page.active && page.in_header) &&    
            <Link key={page._id} href={`/page?page_name=${page.seo_title}`}>
              <Menu.Item onClick={hideMenu} header active={isActive(`/page?page_name=${page.seo_title}`)}>
                {page.title}
              </Menu.Item>
            </Link>          
          })
        }

          {isRootOrAdmin &&
          <>
            <Link href="/pages">
            <Menu.Item onClick={hideMenu} header active={isActive('/pages')}>
              <Icon
                name="clone"
                size="large"
              />
              Pages
            </Menu.Item>
          </Link>          
          </>}

          { props.user ? (<>
          <Link href="/account">
            <Menu.Item onClick={hideMenu} header active={isActive('/account')}>
              <Icon
                name="user"
                size="large"
              />
              Account
            </Menu.Item>
          </Link>
          
          
          <Menu.Item onClick={handleLogout} header>
            <Icon
              name="sign out"
              size="large"
            />
            Logout
          </Menu.Item>  
          </>)
        :
        ( <>
          {/* <Link href="/login">
            <Menu.Item onClick={hideMenu} header active={isActive('/login')}>
              <Icon
                name="sign in"
                size="large"
              />
              Login
            </Menu.Item>
          </Link>        
          
          
          <Link href="/signup">
            <Menu.Item onClick={hideMenu} header active={isActive('/signup')}>
              <Icon
                name="signup"
                size="large"
              />
              Sign Up
            </Menu.Item>
          </Link> */}
          </>
          )
        }
        </Container>
      </Menu>
    </header>
    </>
  );
}

const mapStateToProps = (state) => {
  return {
    user: state.user,
    pages: state.pages,
    settings: state.settings
  }
}

export default connect(mapStateToProps)(Header);