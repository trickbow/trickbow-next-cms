import React from 'react';
import Head from "next/head";
import { Container } from "semantic-ui-react";
import Footer from "./Footer";
import Header from "./Header";
import HeadContent from "./HeadContent";
import "../../static/styles/styles.scss";

function Layout({ children, user, settings }) {

  React.useEffect(()=>{
    window.document.body.className = settings.theme;
  });
  
  return (
    <>
      <Head>
        <HeadContent />
        {/* Stylesheets */}
        <link
          rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css"
        />
        <title>{settings.site_name ? settings.site_name : "Trick-Bow CMS"}</title>
      </Head>
      <Header user={user} settings={settings} />
      <Container id="main-container" style={{ paddingTop: "1em" }}>
        {children}
      </Container>      
      <Footer settings={settings}></Footer>
    </>
  );
}

export default Layout;
