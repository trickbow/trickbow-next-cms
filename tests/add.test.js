const add = (a,b) => a + b;
const generateGreeting = (name = "Anonymous") => `Hello ${name}`

test('should add 2 numbers', () =>{
    const result = add(3, 5);
    expect(result).toBe(3+5);
});

test ('should greet Carol', () =>{
    const result = generateGreeting("Carol");
    expect(result).toBe("Hello Carol");
});

test('should generate greeting for no name', () =>{
    const result = generateGreeting();
    expect(result).toBe("Hello Anonymous")
})