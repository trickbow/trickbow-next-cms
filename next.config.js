// must restart server whenever you make changes in next.config
const withSass = require('@webdeb/next-styles');

module.exports = withSass({
  target: 'serverless',
  sass: true,
  cssModules: true,
  webpack (config, options) {
    config.module.rules.push({
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
        use: {
            loader: 'url-loader',
            options: {
                limit: 100000
            }
        }
    });
    return config;
  },
  env:{
    MONGO_SRV: process.env.MONGO_SRV,
    JWT_SECRET: process.env.JWT_SECRET,
    CLOUDINARY_URL: process.env.CLOUDINARY_URL,
    STRIPE_SECRET_KEY: process.env.STRIPE_SECRET_KEY,
    TINYMCE_API_KEY: process.env.TINYMCE_API_KEY,
    EMAIL_ADDRESS: process.env.EMAIL_ADDRESS,
    EMAIL_PASSWORD: process.env.EMAIL_PASSWORD,
  }
});
